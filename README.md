# Snow Analytics

Snow analytics est un outil de représentation et d'analyse des accidents d'avalanche entre 1995 et 2019. Les accidents représentés ont tous mené à au moins un décès. Ces données sont fournies par le SLF (Institut für Schnee- und LawinenForschung).

## Données
Les données sont constitués de nombreuses avalanches suisse et leurs détails (les spécifications en gras sont celles qui ont été utilisées dans notre représentation):

- **Date**
- Année hydrologique
- Canton
- Localité
- **Coordonnées du départ de l'avalanche**
- **Altitude du départ de l'avalanche**
- **Orientation de la pente**
- **Inclinaison de la pente**
- **Danger d'avalanche annoncé**
- Nombre de morts
- Nombre de personnes prises dans l'avalanche
- Nombre de personnes recouvertes par l'avalanche
- **Activité des personnes prises dans l'avalanche**


## Questions et analyse

La réflexion pour la conception de notre outil a été guidée par les questions et hypothèses suivantes.

### Quelles sont les pentes les plus dangereuses ?

*Puis-je m'aventurer sur cette pente dans ces conditions précises ?*

L'hypothèse démontre les altitudes, orientation et inclinaison des pentes les plus dangereuses. La théorie sur les avalanches spécifie qu'il faut faire attention aux avalanches dans les pentes de plus de 30°. Le graphique sur l'inclinaison de la pente le confirme, on voit bel et bien une augmentation du nombre d'accidents à partir de cette inclinaison.

![](https://i.imgur.com/B0sDRqo.png)

L'altitude de la pente n'est pas un facteur. En effet les avalanches se produisent aux altitudes où il y a de la neige en quantité. De plus les avalanches sont déclenchées principalement par des personnes (les avalanches spontanées sont extrêmement rare). Par conséquent, les altitudes dangereuses sont *dangereuses* car ce sont les altitudes où les gens skient.

![](https://i.imgur.com/GbB0KoX.png)

Le fait que les pentes face nord sont plus dangereuses n'est visiblement pas une légende. Comme le montre le graphique suivant, les pentes dangereuses (les pentes avec le plus d'accident) sont principalement orientées nord.

![](https://i.imgur.com/iSIwfIF.png)


### Les types de skieurs jugent-ils différemment les conditions ?

La première approche a été de comparer les accidents entre les types de skieurs et séparé par danger annoncé. Il faut cependant rappeler que nous ne connaissons pas le nombre total des pratiquants de ces sports d'hiver. Cette représentation démontre qu'il y a plus d'accidents chez les personnes pratiquant de la randonnée à ski. Cette représentation sépare les accidents par tranche de 8 ans. On peut clairement voir une augmentation du nombre d'accidents ces dernières années. Probablement à cause de l'augmentation des pratiquants de ces sports. 

![](https://i.imgur.com/0NMf4PU.png)

La deuxième représentation permet mieux de différencier la prise de risque entre les pratiquants de hors-piste et les randonneurs:

![](https://i.imgur.com/rYIsTpe.png)

L'indice représente la moyenne des accidents de l'année pondérée par le danger. Si cet indice est haut, les accidents qui se sont passé cette année sont principalement arrivé lors d'un danger d'avalanche plus haut. Il y a une différence entre les skieurs *hors-piste* et les randonneurs. On pourrait dire que les skieurs *hors-piste* prennent moins compte du danger d'avalanche annoncé.

Il est difficile avec les données d'accident d'évaluer la capacité des skieurs à évaluer les conditions. 

### Les gens sont-ils mieux éduqués qu'auparavant ?

Ou *Prennent-ils plus de risques ?*

L'augmentation du nombre de randonneurs à ski, des équipements toujours plus pointus (DVA, sac airbag) et de la forte médiatisation des accidents de montagne laissent penser qu'il y a eu une prise de conscience ces dernières années. Nous avons voulu vérifier cette hypothèse grâce aux données à disposition.

Notre idée est de vérifier au fil du temps à quel niveau de danger d'avalanche les accidents se sont produits. S'aventurer sur des pentes dangereuses lorsque le niveau d'avalanche et marqué ou fort est souvent imprudent.

La première représentation montre que la majorité des accidents se produisent à un danger *marqué*, mais il y a eu peu d'évolution au cours du temps.

![](https://i.imgur.com/0lfcySb.png)

La deuxième représentation montre le niveau d'avalanche moyen (pondéré) par année. Là encore, la courbe est stable et ne montre pas de tendance (dé)croissante.

![](https://i.imgur.com/kAHCYYT.png)

Cette hypothèse n'est donc pas validée, nous n'avons pas remarqué d'évolution dans le comportement des skieurs au fil du temps.

## Choix de la représentation

Le premier onglet "Explorer" offre une vue exploratoire. La carte permet de regarder un historique des avalanches. L'utilisateur peut cliquer sur une avalanche dans un lieu qu'il connait et inspecter les détails.

Les filtres permettent de réduire les points sur la carte et donc de rendre la visualisation dynamique.

L'onglet "Détailler" permet de voir les données dans différents graphiques. De plus, il y a la possibilité d'afficher nos hypothèses dans une liste déroulante.
Ce choix d'hypothèses permet de réduire l'affichage au message que l'on veut faire passer.
Dans cet onglet, il y a également les filtres. Ils permettent de confirmer de nombreuses hypothèses en filtrant les données.

### Calcul du nombre d'accidents pondéré par le danger

Le graphe présentant le *danger d'avalanche pondéré* permet de montrer le danger d'avalanche *moyen* pour les accidents au cours d'une année.

![](https://i.imgur.com/kAHCYYT.png)

Le calcul est le suivant pour chaque année. Le danger moyen est de 3, on initialise donc la valeur à 3. Pour chaque niveau de danger, on modifie cette valeur en ajoutant `(dangerLevel - 3) * (nbAccidentAtDangerLevel / nbAccidentTotal)`.

Nous obtenons donc le calcul suivant pour chaque année.
```
result = 3
for dangerLevel in allDangerLevels
    diffDangerLevel = dangerLevel - 3
    proportionAccident = nbAccidentAtDangerLevel / nbAccidentTotal
    result += diffDangerLevel * proportionAccident
```
Cela nous donne une valeur entre 1 (faible) et 5 (très fort) correspondant au danger moyen.

## Technologies utilisées

**Vue.js**
Utilisé pour la gestion des états: donnée filtrées, état des filtres.
Utilisé pour que le site soit une Single Page Application.

**Chart.js**
Utilisé pour presque tous les graphes, dans sa version *vue-chartjs* afin de réagir dynamiquement aux filtres. Propose de très nombreuses options et est très simple à utiliser.

**ApexCharts**
Uniquement utilisé pour le graphe *heatmap* qui n'était pas disponible avec ChartJs. Utilisé dans sa version *vue-apexcharts* afin de réagir dynamiquement aux filtres. Propose de nombreuses options et l'utilisation est simple et efficace.

**Python Notebook**
Utilisé pour la mise en place des hypothèses et pour une pré-exploration du dataset. Cet outil a été utilisé lors de la première semaine du projet.

**Leaflet**
Utilisé pour la carte de l'onglet "Explorer". Il permet de placer des points sur la carte. Ainsi que pour ajouter une légende et un tooltip aux points.
