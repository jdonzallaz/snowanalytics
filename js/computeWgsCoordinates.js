import { data } from "./data.js";

for (const [i, value] of data.entries()) {
  const wgs = await lv03towgs84(
    i["start.zone.coordinates.x"],
    i["start.zone.coordinates.y"],
    i["start.zone.elevation"]
  );
  data[i]["easting"] = wgs.easting;
  data[i]["northing"] = wgs.northing;
}
console.log(data);

async function lv03towgs84(e, n, a) {
  const u = `http://geodesy.geo.admin.ch/reframe/lv03towgs84?easting=${e}&northing=${n}&altitude=${a}&format=json`;
  const response = await fetch(u);
  const json_data = await response.json();

  const easting = json_data["easting"];
  const northing = json_data["northing"];

  return { easting, northing };
}
