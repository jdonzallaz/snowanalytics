import { dangerLevels } from "./../const.js";

export default {
  name: "Map",
  components: {},
  computed: {
    data: function() {
      return this.$store.state.filteredData;
    }
  },
  mounted: function() {
    if (map != undefined) map.remove();
    map = L.map("map", { minZoom: 8, maxZoom: 12 }).setView([46.8, 8.33], 8);
    map.addLayer(Stamen_Terrain);
    setTimeout(map.invalidateSize.bind(map));
    add(this.data);
    addLegend();
  },
  watch: {
    data: function() {
      for (var i = 0; i < markers.length; i++) {
        map.removeLayer(markers[i]);
      }

      markers = [];
      add(this.data);
    }
  },
  template: `
    <div class="col-md-10">
        <div id="map"></div>
    </div>
  `
};

var markers = [];
var map;
var Stamen_Terrain = L.tileLayer(
  "https://stamen-tiles-{s}.a.ssl.fastly.net/terrain/{z}/{x}/{y}{r}.{ext}",
  {
    attribution:
      'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    minZoom: 0,
    maxZoom: 18,
    ext: "png"
  }
);

function add(data) {
  for (const value of data) addMarkerWithPopup(value);
}

function addLegend() {
  var legend = L.control({ position: "topright" });

  legend.onAdd = function(map) {
    var div = L.DomUtil.create("div", "map-legend");

    div.innerHTML += `
        <b class="d-inline-block mb-1">Danger d'avalanche :</b><br>
      `;

    for (const dangerLevel of dangerLevels.sort(
      (d1, d2) => d1.value < d2.value
    ))
      div.innerHTML += `
        <span class="badge badge-pill map-legend-color rounded" style="background-color: ${dangerLevel.color}"></span>
        <span class="map-legend-text">${dangerLevel.name}</span><br>
      `;

    div.innerHTML += `
      <span class="badge badge-pill map-legend-color rounded" style="background-color: #ffffff"></span>
      <span class="map-legend-text">Non défini</span><br>
    `;

    return div;
  };

  legend.addTo(map);
}

function addMarkerWithPopup(i) {
  let dangerLevel = i["forecasted.dangerlevel"];
  if (dangerLevel == "NA") dangerLevel = "-";
  else {
    dangerLevel = dangerLevels.find(d => d.value === dangerLevel);
    dangerLevel.fontColor = "#000";
  }

  const circle = L.circleMarker([i.northing, i.easting], {
    radius: 3,
    color: dangerLevel == "-" ? "#ffffff" : dangerLevel.color,
    fill: true,
    fillOpacity: 1
  });

  if (dangerLevel instanceof Object) {
    if (dangerLevel.value >= 4) dangerLevel.fontColor = "#fff";
    dangerLevel = `
    <span class="badge" style="background-color: ${dangerLevel.color}; color: ${dangerLevel.fontColor}">
        ${dangerLevel.name}
    </span>`;
  }

  const date = moment(i.date).format("LL");
  const location = i["local.name"];
  let altitude = i["start.zone.elevation"];
  if (altitude != "NA") altitude += " m";
  else altitude = "-";
  const orientation = i["start.zone.slope.aspect"];
  let inclination = i["start.zone.inclination"];
  if (inclination != "NA") inclination += "°";
  else inclination = "-";
  const deaths = i["number.dead"];
  const caughts = i["number.caught"];
  const burried = i["number.fully.buried"];
  const activity = i["activity"];

  circle.bindPopup(`
<div class="d-flex"><b>${location}</b><span class="ml-auto text-muted">${date}</span></div>
<b>Altitude :</b> ${altitude}
<br>
<b>Orientation de la pente :</b> ${orientation}
<br>
<b>Inclinaison de la pente :</b> ${inclination}
<br>
<b>Danger d'avalanche prévu :</b> ${dangerLevel}
<br>
<b>Nombre de décès :</b> ${deaths}
<br>
<b>Nombre de personnes prises :</b> ${caughts}
<br>
<b>Nombre de personnes complètement ensevelies :</b> ${burried}
<br>
<b>Activité :</b> ${activity}
<br>
`);
  markers.push(circle);
  circle.addTo(map);
}
