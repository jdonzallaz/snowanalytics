import YearFilter from "./filters/YearFilter.js";
import AltitudeFilter from "./filters/AltitudeFilter.js";
import InclinationFilter from "./filters/InclinationFilter.js";
import DangerFilter from "./filters/DangerFilter.js";
import ActivityFilter from "./filters/ActivityFilter.js";

export default {
  name: "Filters",
  components: {YearFilter, AltitudeFilter, InclinationFilter, 
    DangerFilter, ActivityFilter },
  created: function () {
    if (!this.$store.state.filtersInit.init)
      this.$store.commit('initFilters');
  },
  methods:{
    reset:  function (event){
      this.$store.commit('resetFilters');
    }
  },
  template: `
    <div class="col-md-2 pt-3">
        <h3 class="d-flex">
          Filtres
          <button type="button" class="btn btn-secondary ml-auto" v-on:click="reset">Réinitialiser</button>
        </h3>
        <hr/>
            <h5>Activités</h5>
            <activity-filter/>
        <hr/>
            <h5>Niveau de danger</h5>
            <danger-filter />
            <br>
        <hr/>
            <h5>Années</h5>
            <year-filter />
            <br>
        <hr/>
            <h5>Altitude</h5>
            <altitude-filter />
            <br>
        <hr/>
            <h5>Pente</h5>
            <inclination-filter />
            <br>
            <br>
    </div>
      `
};