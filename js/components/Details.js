import ActivityToDanger from "./charts/ActivityToDanger.js";
import Altitude from "./charts/Altitude.js";
import DangerLevelToDate from "./charts/DangerLevelToDate.js";
import InclinationChart from "./charts/InclinationChart.js";
import Hypothesis from "./Hypothesis.js";
import Orientation from "./charts/Orientation.js";
import DangerToDeathToYear from "./charts/DangerToDeathToYear.js";
import Filters from "./Filters.js";

export default {
  name: "Details",
  components: {
    ActivityToDanger,
    Altitude,
    DangerLevelToDate,
    Filters,
    Hypothesis,
    Orientation,
    InclinationChart,
    DangerToDeathToYear
  },
  computed: {
    hypothesis: function() {
      return this.$store.state.selectedHypothesis;
    }
  },
  template: `
      <div class="container-fluid">
        <hypothesis/>
        <div v-if="hypothesis == 0" class="row mt-3 border-top">
          <filters />
          <!-- Hypothèse 0: Exploration ! FINI -->
          <div class="col-md-10 pl-3 border-left py-3">
            <div class="row">
              <div class="col-md-6 mb-3">
                <div class="chart-title">Danger d'avalanche</div>
                <div class="chart-subtitle">Nombre d'accidents selon danger d'avalanche prévu, par année</div>
                <danger-level-to-date/>
              </div>
              <div class="col-md-6 mb-3">
                <div class="chart-title">Orientation</div>
                <div class="chart-subtitle">Nombre d'accidents selon l'orientation de la pente</div>
                <orientation/>
              </div>
              <div class="col-md-6 mb-3">
                <div class="chart-title">Danger d'avalanche</div>
                <div class="chart-subtitle">Nombre d'accidents selon danger d'avalanche prévu</div>
                <activity-to-danger />
              </div>
              <div class="col-md-6 mb-3">
                <div class="chart-title">Altitude</div>
                <div class="chart-subtitle">Nombre d'accidents selon l'altitude de départ de l'avalanche</div>
                <altitude :separated="true" :ymax="25"/>
              </div>
              <div class="col-md-6 mb-3">
                <div class="chart-title">Inclinaison</div>
                <div class="chart-subtitle">Nombre d'accidents selon l'inclinaison de la pente</div>
                <inclination-chart :separated="true" :ymax="25"/>
              </div>
              <div class="col-md-6 mb-3">
                <div class="chart-title">Danger d'avalanche pondéré</div>
                <div class="chart-subtitle">Nombre d'accidents pondérés par le danger d'avalanche prévu</div>
                <danger-to-death-to-year :separated="true"/>
              </div>
            </div>
          </div>
        </div>
        <!-- Hypothèse 1: Quelles sont les pentes dangereuses ? FINI -->
        <div v-if="hypothesis == 1" class="row mt-3 border-top">
          <filters />
          <div class="col-md-10 pl-3 border-left py-3">
            <div class="row">
              <div class="col-md-8 mb-3">
              <div class="chart-title">Altitude</div>
                <div class="chart-subtitle">Nombre d'accidents selon l'altitude de départ de l'avalanche</div>
                <altitude :separated="false"/>
              </div>
              <div class="col-md-4 mb-3">
                <div class="chart-title">Orientation</div>
                <div class="chart-subtitle">Nombre d'accidents selon l'orientation de la pente</div>
                <orientation/>
              </div>
              <div class="col-md-8 mb-3">
                <div class="chart-title">Inclinaison</div>
                <div class="chart-subtitle">Nombre d'accidents selon l'inclinaison de la pente</div>
                <inclination-chart :separated="false"/>
              </div>
            </div>
          </div>
        </div>
        <!-- Hypothèse 2: Types de skieurs et danger ? FINI -->
        <div v-if="hypothesis == 2" class="row mt-3 border-top">
          <filters />
          <div class="col-md-10 pl-3 border-left py-3">
            <div class="row">
              <div class="col-md-4 mb-3">
                <div class="chart-title">Danger d'avalanche</div>
                <div class="chart-subtitle">Nombre d'accidents selon danger d'avalanche prévu, entre 1995 et 2003</div>
                <activity-to-danger :year="[1995,2003]" :ymax="65"/>
              </div>
              <div class="col-md-4 mb-3">
                <div class="chart-title">Danger d'avalanche</div>
                <div class="chart-subtitle">Nombre d'accidents selon danger d'avalanche prévu, entre 2003 et 2011</div>
                <activity-to-danger :year="[2003,2011]" :ymax="65"/>
              </div>
              <div class="col-md-4 mb-3">
                <div class="chart-title">Danger d'avalanche</div>
                <div class="chart-subtitle">Nombre d'accidents selon danger d'avalanche prévu, entre 2011 et 2019</div>
                <activity-to-danger :year="[2011,2019]" :ymax="65"/>
              </div>
              <div class="col-md-12 mb-3">
                <div class="chart-title">Danger d'avalanche pondéré</div>
                <div class="chart-subtitle">Nombre d'accidents pondérés par le danger d'avalanche prévu</div>
                <!--<altitude :separated="true"/>-->
                <danger-to-death-to-year :separated="true"/>
              </div>
            </div>
          </div>
        </div>
        <!-- Hypothèse 3: Les skieurs sont-ils mieux formés ajd ? FINI -->
        <div v-if="hypothesis == 3" class="row mt-3 border-top">
          <filters />
          <div class="col-md-10 pl-3 border-left py-3">
            <div class="row">
              <div class="col-md-12 mb-3">
                <div class="chart-title">Danger d'avalanche</div>
                <div class="chart-subtitle">Nombre d'accidents selon danger d'avalanche prévu, par année</div>
                <danger-level-to-date/>
              </div>
              <div class="col-md-12 mb-3">
                <div class="chart-title">Danger d'avalanche pondéré</div>
                <div class="chart-subtitle">Nombre d'accidents pondérés par le danger d'avalanche prévu</div>
                <danger-to-death-to-year :separated="false"/>
              </div>
            </div>
          </div>
        </div>
      </div>
      `
};
