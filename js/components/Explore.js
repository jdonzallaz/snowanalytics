import Map from "./Map.js";
import Filters from "./Filters.js";

export default {
  name: "Explore",
  components: { Map, Filters },
  template: `
    <div class="container-fluid">
      <div class="row">
        <filters />
        <Map/>
      </div>
    </div>
    `
};
