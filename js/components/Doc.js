export default {
  name: "Doc",
  template: `
<div id="doc" class="markdown-body container comment-enabled" data-hard-breaks="true" style="position: relative;"><h1 id="Snow-Analytics"><a class="anchor hidden-xs" href="#Snow-Analytics" title="Snow-Analytics"><span class="octicon octicon-link"></span></a>Snow Analytics</h1><p>Snow analytics est un outil de représentation et d’analyse des accidents d’avalanche entre 1995 et 2019. Les accidents représentés ont tous mené à au moins un décès. Ces données sont fournies par le SLF (Institut für Schnee- und LawinenForschung).</p><h2 id="Données"><a class="anchor hidden-xs" href="#Données" title="Données"><span class="octicon octicon-link"></span></a>Données</h2><p>Les données sont constitués de nombreuses avalanches suisse et leurs détails (les spécifications en gras sont celles qui ont été utilisées dans notre représentation):</p><ul>
<li><strong>Date</strong></li>
<li>Année hydrologique</li>
<li>Canton</li>
<li>Localité</li>
<li><strong>Coordonnées du départ de l’avalanche</strong></li>
<li><strong>Altitude du départ de l’avalanche</strong></li>
<li><strong>Orientation de la pente</strong></li>
<li><strong>Inclinaison de la pente</strong></li>
<li><strong>Danger d’avalanche annoncé</strong></li>
<li>Nombre de morts</li>
<li>Nombre de personnes prises dans l’avalanche</li>
<li>Nombre de personnes recouvertes par l’avalanche</li>
<li><strong>Activité des personnes prises dans l’avalanche</strong></li>
</ul><h2 id="Questions-et-analyse"><a class="anchor hidden-xs" href="#Questions-et-analyse" title="Questions-et-analyse"><span class="octicon octicon-link"></span></a>Questions et analyse</h2><p>La réflexion pour la conception de notre outil a été guidée par les questions et hypothèses suivantes.</p><h3 id="Quelles-sont-les-pentes-les-plus-dangereuses-"><a class="anchor hidden-xs" href="#Quelles-sont-les-pentes-les-plus-dangereuses-" title="Quelles-sont-les-pentes-les-plus-dangereuses-"><span class="octicon octicon-link"></span></a>Quelles sont les pentes les plus dangereuses ?</h3><p><em>Puis-je m’aventurer sur cette pente dans ces conditions précises ?</em></p><p>L’hypothèse démontre les altitudes, orientation et inclinaison des pentes les plus dangereuses. La théorie sur les avalanches spécifie qu’il faut faire attention aux avalanches dans les pentes de plus de 30°. Le graphique sur l’inclinaison de la pente le confirme, on voit bel et bien une augmentation du nombre d’accidents à partir de cette inclinaison.</p><p><img src="https://i.imgur.com/B0sDRqo.png" alt=""></p><p>L’altitude de la pente n’est pas un facteur. En effet les avalanches se produisent aux altitudes où il y a de la neige en quantité. De plus les avalanches sont déclenchées principalement par des personnes (les avalanches spontanées sont extrêmement rare). Par conséquent, les altitudes dangereuses sont <em>dangereuses</em> car ce sont les altitudes où les gens skient.</p><p><img src="https://i.imgur.com/GbB0KoX.png" alt=""></p><p>Le fait que les pentes face nord sont plus dangereuses n’est visiblement pas une légende. Comme le montre le graphique suivant, les pentes dangereuses (les pentes avec le plus d’accident) sont principalement orientées nord.</p><p><img src="https://i.imgur.com/iSIwfIF.png" alt=""></p><h3 id="Les-types-de-skieurs-jugent-ils-différemment-les-conditions-"><a class="anchor hidden-xs" href="#Les-types-de-skieurs-jugent-ils-différemment-les-conditions-" title="Les-types-de-skieurs-jugent-ils-différemment-les-conditions-"><span class="octicon octicon-link"></span></a>Les types de skieurs jugent-ils différemment les conditions ?</h3><p>La première approche a été de comparer les accidents entre les types de skieurs et séparé par danger annoncé. Il faut cependant rappeler que nous ne connaissons pas le nombre total des pratiquants de ces sports d’hiver. Cette représentation démontre qu’il y a plus d’accidents chez les personnes pratiquant de la randonnée à ski. Cette représentation sépare les accidents par tranche de 8 ans. On peut clairement voir une augmentation du nombre d’accidents ces dernières années. Probablement à cause de l’augmentation des pratiquants de ces sports.</p><p><img src="https://i.imgur.com/0NMf4PU.png" alt=""></p><p>La deuxième représentation permet mieux de différencier la prise de risque entre les pratiquants de hors-piste et les randonneurs:</p><p><img src="https://i.imgur.com/rYIsTpe.png" alt=""></p><p>L’indice représente la moyenne des accidents de l’année pondérée par le danger. Si cet indice est haut, les accidents qui se sont passé cette année sont principalement arrivé lors d’un danger d’avalanche plus haut. Il y a une différence entre les skieurs <em>hors-piste</em> et les randonneurs. On pourrait dire que les skieurs <em>hors-piste</em> prennent moins compte du danger d’avalanche annoncé.</p><p>Il est difficile avec les données d’accident d’évaluer la capacité des skieurs à évaluer les conditions.</p><h3 id="Les-gens-sont-ils-mieux-éduqués-qu’auparavant-"><a class="anchor hidden-xs" href="#Les-gens-sont-ils-mieux-éduqués-qu’auparavant-" title="Les-gens-sont-ils-mieux-éduqués-qu’auparavant-"><span class="octicon octicon-link"></span></a>Les gens sont-ils mieux éduqués qu’auparavant ?</h3><p>Ou <em>Prennent-ils plus de risques ?</em></p><p>L’augmentation du nombre de randonneurs à ski, des équipements toujours plus pointus (DVA, sac airbag) et de la forte médiatisation des accidents de montagne laissent penser qu’il y a eu une prise de conscience ces dernières années. Nous avons voulu vérifier cette hypothèse grâce aux données à disposition.</p><p>Notre idée est de vérifier au fil du temps à quel niveau de danger d’avalanche les accidents se sont produits. S’aventurer sur des pentes dangereuses lorsque le niveau d’avalanche et marqué ou fort est souvent imprudent.</p><p>La première représentation montre que la majorité des accidents se produisent à un danger <em>marqué</em>, mais il y a eu peu d’évolution au cours du temps.</p><p><img src="https://i.imgur.com/0lfcySb.png" alt=""></p><p>La deuxième représentation montre le niveau d’avalanche moyen (pondéré) par année. Là encore, la courbe est stable et ne montre pas de tendance (dé)croissante.</p><p><img src="https://i.imgur.com/kAHCYYT.png" alt=""></p><p>Cette hypothèse n’est donc pas validée, nous n’avons pas remarqué d’évolution dans le comportement des skieurs au fil du temps.</p><h2 id="Choix-de-la-représentation"><a class="anchor hidden-xs" href="#Choix-de-la-représentation" title="Choix-de-la-représentation"><span class="octicon octicon-link"></span></a>Choix de la représentation</h2><p>Le premier onglet “Explorer” offre une vue exploratoire. La carte permet de regarder un historique des avalanches. L’utilisateur peut cliquer sur une avalanche dans un lieu qu’il connait et inspecter les détails.</p><p>Les filtres permettent de réduire les points sur la carte et donc de rendre la visualisation dynamique.</p><p>L’onglet “Détailler” permet de voir les données dans différents graphiques. De plus, il y a la possibilité d’afficher nos hypothèses dans une liste déroulante.<br>
Ce choix d’hypothèses permet de réduire l’affichage au message que l’on veut faire passer.<br>
Dans cet onglet, il y a également les filtres. Ils permettent de confirmer de nombreuses hypothèses en filtrant les données.</p><h3 id="Calcul-du-nombre-d’accidents-pondéré-par-le-danger"><a class="anchor hidden-xs" href="#Calcul-du-nombre-d’accidents-pondéré-par-le-danger" title="Calcul-du-nombre-d’accidents-pondéré-par-le-danger"><span class="octicon octicon-link"></span></a>Calcul du nombre d’accidents pondéré par le danger</h3><p>Le graphe présentant le <em>danger d’avalanche pondéré</em> permet de montrer le danger d’avalanche <em>moyen</em> pour les accidents au cours d’une année.</p><p><img src="https://i.imgur.com/kAHCYYT.png" alt=""></p><p>Le calcul est le suivant pour chaque année. Le danger moyen est de 3, on initialise donc la valeur à 3. Pour chaque niveau de danger, on modifie cette valeur en ajoutant <code>(dangerLevel - 3) * (nbAccidentAtDangerLevel / nbAccidentTotal)</code>.</p><p>Nous obtenons donc le calcul suivant pour chaque année.</p><pre><code>result = 3
for dangerLevel in allDangerLevels
    diffDangerLevel = dangerLevel - 3
    proportionAccident = nbAccidentAtDangerLevel / nbAccidentTotal
    result += diffDangerLevel * proportionAccident
</code></pre><p>Cela nous donne une valeur entre 1 (faible) et 5 (très fort) correspondant au danger moyen.</p><h2 id="Technologies-utilisées"><a class="anchor hidden-xs" href="#Technologies-utilisées" title="Technologies-utilisées"><span class="octicon octicon-link"></span></a>Technologies utilisées</h2><p><strong>Vue.js</strong><br>
Utilisé pour la gestion des états: donnée filtrées, état des filtres.<br>
Utilisé pour que le site soit une Single Page Application.</p><p><strong>Chart.js</strong><br>
Utilisé pour presque tous les graphes, dans sa version <em>vue-chartjs</em> afin de réagir dynamiquement aux filtres. Propose de très nombreuses options et est très simple à utiliser.</p><p><strong>ApexCharts</strong><br>
Uniquement utilisé pour le graphe <em>heatmap</em> qui n’était pas disponible avec ChartJs. Utilisé dans sa version <em>vue-apexcharts</em> afin de réagir dynamiquement aux filtres. Propose de nombreuses options et l’utilisation est simple et efficace.</p><p><strong>Python Notebook</strong><br>
Utilisé pour la mise en place des hypothèses et pour une pré-exploration du dataset. Cet outil a été utilisé lors de la première semaine du projet.</p><p><strong>Leaflet</strong><br>
Utilisé pour la carte de l’onglet “Explorer”. Il permet de placer des points sur la carte. Ainsi que pour ajouter une légende et un tooltip aux points.</p></div>
    <div class="ui-toc dropup unselectable hidden-print" style="display:none;">
        <div class="pull-right dropdown">
            <a id="tocLabel" class="ui-toc-label btn btn-default" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" title="Table of content">
                <i class="fa fa-bars"></i>
            </a>
            <ul id="ui-toc" class="ui-toc-dropdown dropdown-menu" aria-labelledby="tocLabel">
                <div class="toc"><ul class="nav">
<li class=""><a href="#Snow-Analytics" title="Snow Analytics">Snow Analytics</a><ul class="nav">
<li><a href="#Données" title="Données">Données</a></li>
<li><a href="#Questions-et-analyse" title="Questions et analyse">Questions et analyse</a><ul class="nav">
<li><a href="#Quelles-sont-les-pentes-les-plus-dangereuses-" title="Quelles sont les pentes les plus dangereuses ?">Quelles sont les pentes les plus dangereuses ?</a></li>
<li><a href="#Les-types-de-skieurs-jugent-ils-différemment-les-conditions-" title="Les types de skieurs jugent-ils différemment les conditions ?">Les types de skieurs jugent-ils différemment les conditions ?</a></li>
<li><a href="#Les-gens-sont-ils-mieux-éduqués-qu’auparavant-" title="Les gens sont-ils mieux éduqués qu’auparavant ?">Les gens sont-ils mieux éduqués qu’auparavant ?</a></li>
</ul>
</li>
<li class=""><a href="#Choix-de-la-représentation" title="Choix de la représentation">Choix de la représentation</a><ul class="nav">
<li class=""><a href="#Calcul-du-nombre-d’accidents-pondéré-par-le-danger" title="Calcul du nombre d’accidents pondéré par le danger">Calcul du nombre d’accidents pondéré par le danger</a></li>
</ul>
</li>
<li><a href="#Technologies-utilisées" title="Technologies utilisées">Technologies utilisées</a></li>
</ul>
</li>
</ul>
</div><div class="toc-menu"><a class="expand-toggle" href="#">Expand all</a><a class="back-to-top" href="#">Back to top</a><a class="go-to-bottom" href="#">Go to bottom</a></div>
            </ul>
        </div>
    </div>
    <div id="ui-toc-affix" class="ui-affix-toc ui-toc-dropdown unselectable hidden-print" data-spy="affix" style="top:17px;display:none;" null null>
        <div class="toc"><ul class="nav">
<li class=""><a href="#Snow-Analytics" title="Snow Analytics">Snow Analytics</a><ul class="nav">
<li><a href="#Données" title="Données">Données</a></li>
<li><a href="#Questions-et-analyse" title="Questions et analyse">Questions et analyse</a><ul class="nav">
<li><a href="#Quelles-sont-les-pentes-les-plus-dangereuses-" title="Quelles sont les pentes les plus dangereuses ?">Quelles sont les pentes les plus dangereuses ?</a></li>
<li><a href="#Les-types-de-skieurs-jugent-ils-différemment-les-conditions-" title="Les types de skieurs jugent-ils différemment les conditions ?">Les types de skieurs jugent-ils différemment les conditions ?</a></li>
<li><a href="#Les-gens-sont-ils-mieux-éduqués-qu’auparavant-" title="Les gens sont-ils mieux éduqués qu’auparavant ?">Les gens sont-ils mieux éduqués qu’auparavant ?</a></li>
</ul>
</li>
<li class=""><a href="#Choix-de-la-représentation" title="Choix de la représentation">Choix de la représentation</a><ul class="nav">
<li class=""><a href="#Calcul-du-nombre-d’accidents-pondéré-par-le-danger" title="Calcul du nombre d’accidents pondéré par le danger">Calcul du nombre d’accidents pondéré par le danger</a></li>
</ul>
</li>
<li><a href="#Technologies-utilisées" title="Technologies utilisées">Technologies utilisées</a></li>
</ul>
</li>
</ul>
</div><div class="toc-menu"><a class="expand-toggle" href="#">Expand all</a><a class="back-to-top" href="#">Back to top</a><a class="go-to-bottom" href="#">Go to bottom</a></div>
    </div>
    `
};
