import { getMinY, getMaxY } from "../../utils.js";

export default {
  name: "YearFilter",
  components: {},

  computed: {
    minMaxYear() {        
        return this.$store.state.filtersInit.year
    },
    yearRange: {
        get() {
            return this.$store.state.filters.year
        },
        set(value) {
          this.$store.commit('setFilter', { filter: "year", value });
        },
    },
  },
  template: `
    <div class="px-3">
      <vue-slider v-model="yearRange" :min="minMaxYear[0]" :max="minMaxYear[1]" :marks="true" :enable-cross="true" :interval="1" :tooltip="'always'" :tooltip-placement="'bottom'"
      :contained="true" :silent="true" :hide-label="true" :lazy="true"></vue-slider>
    </div>
  `
};