export default {
  name: "ActivityFilter",
  components: {},
  computed: {
    offPiste: {
      get() {
          return this.$store.state.filters.activity.offpiste;
      },
      set(value) {
          this.$store.commit('setFilter', { filter: "activity", activity: 'offpiste', value });
      },
    },
    tour: {
      get() {
          return this.$store.state.filters.activity.tour;
      },
      set(value) {
          this.$store.commit('setFilter', { filter: "activity", activity: 'tour', value });
      },
    },
    other: {
      get() {
          return this.$store.state.filters.activity.other;
      },
      set(value) {
          this.$store.commit('setFilter', { filter: "activity", activity: 'other', value });
      },
    }
  },
  template: 
      `
      <div>
        <div class="custom-control custom-checkbox">
          <input type="checkbox" class="custom-control-input" id="tour" v-model="tour">
          <label class="custom-control-label" for="tour">Randonnée</label>
        </div>

        <div class="custom-control custom-checkbox">
          <input type="checkbox" class="custom-control-input" id="offpiste" v-model="offPiste"/>
          <label class="custom-control-label" for="offpiste">Hors-piste</label>
        </div>

        <div class="custom-control custom-checkbox">
          <input type="checkbox" class="custom-control-input" id="other" v-model="other">
          <label class="custom-control-label" for="other">Autres</label>
        </div>
      </div>
      `
};