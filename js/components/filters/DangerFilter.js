export default {
  name: "DangerFilter",
  components: {},
  computed: {
    minMaxDanger() {
        return this.$store.state.filtersInit.danger
    },
    dangerLevelRange: {
        get() {
            return this.$store.state.filters.danger
        },
        set(value) {
            this.$store.commit('setFilter', { filter: "danger", value });
        },
    },
  },
  data: function () {
    return {
      formatter2: v => {
          switch(v){
            case 1:
                return "faible";
            case 2:
                return "limité";
            case 3:
                return "marqué";
            case 4:
                return "fort";
            case 5:
                return "très fort";
          }
        },
    }
  },
  template: `
    <div class="px-3">
      <vue-slider v-model="dangerLevelRange" :min="minMaxDanger[0]" :max="minMaxDanger[1]" :marks="true" :enable-cross="true" :interval="1"   :tooltip="'always'" :tooltip-placement="'bottom'"
      :tooltip-formatter="formatter2" :contained="true" :silent="true" :hide-label="true"  :lazy="true"/>
    </div>
  `
};