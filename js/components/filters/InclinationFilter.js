import { getMinY, getMaxY } from "../../utils.js";

export default {
  name: "InclinationFilter",
  components: {},
  computed: {
    minMaxInclination() {
      return this.$store.state.filtersInit.inclination;
    },
    inclinationRange: {
      get() {
          return this.$store.state.filters.inclination;
      },
      set(value) {
          this.$store.commit('setFilter', { filter: "inclination", value });
      },
    }
  },
  template: `
    <div class="px-3">
      <vue-slider v-model="inclinationRange" :min="minMaxInclination[0]" :max="minMaxInclination[1]" :enable-cross="true" :interval="1" :tooltip="'always'" :tooltip-placement="'bottom'" :tooltip-formatter="val => val + '°'"
      :contained="true" :silent="true" :hide-label="true" :lazy="true"></vue-slider>
    </div>
  `
};