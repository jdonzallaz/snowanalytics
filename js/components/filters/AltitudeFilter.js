import { getMinY, getMaxY } from "../../utils.js";

export default {
  name: "AltitudeFilter",
  components: {},
  computed: {
    minMaxAltitude() {
      return this.$store.state.filtersInit.altitude
    },
    altitudeRange: {
      get() {
        return this.$store.state.filters.altitude
      },
      set(value) {
        this.$store.commit('setFilter', { filter: "altitude", value });
      },
    }
  },
  template: `
    <div class="px-3">
      <vue-slider v-model="altitudeRange" :min="minMaxAltitude[0]" :max="minMaxAltitude[1]" :enable-cross="true" :interval="10" :tooltip="'always'" :tooltip-placement="'bottom'" :tooltip-formatter="val => val + 'm'"
      :contained="true" :silent="true" :hide-label="true" :lazy="true"/>
    </div>
  `
};