export default {
  name: "Hypothesis",
  computed: {
    selected: {
      get() {
        return this.$store.state.hypothesis.find(
          h => h.id === this.$store.state.selectedHypothesis
        );
      },
      set(value) {
        this.$store.commit("selectHypothesis", { id: value.id });
      }
    },
    ...Vuex.mapState({
      hypothesis: state => state.hypothesis
    })
  },
  template: `
    <div class="mx-auto" style="max-width: 800px;">
        <div class="mb-1 text-muted font-weight-bold">Choisir une hypothèse pour guider l'analyse :</div>
        <select class="custom-select custom-select-lg " v-model="selected">
            <option v-for="option in hypothesis" v-bind:value="option">
                {{ option.title }}
            </option>
        </select>
    </div>
    `
};
