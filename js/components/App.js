export default {
  name: "App",
  components: {},
  mounted: function() {},
  data: () => ({}),
  computed: {
    data() {},
    ...Vuex.mapState({})
  },
  methods: {
    // toggleLed: function(sensorId, state) {
    //   this.$store.commit("toggleLed", {
    //     sensorId: sensorId,
    //     value: state
    //   });
    // }
  },
  template: `
    <div class="root">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="#">Snow Analytics</a>
            <div class="navbar-nav">
                <router-link class="nav-item nav-link" active-class="active" to="/explore">Explorer</router-link>
                <router-link class="nav-item nav-link" active-class="active" to="/details">Détailler</router-link>
                <router-link class="nav-item nav-link" active-class="active" to="/doc">Expliquer</router-link>
            </div>
        </nav>
        <div class="main pt-3">
            <router-view></router-view>
        </div>
        <div class="container-fluid footer py-2 text-right bg-dark text-white">
            Source de toutes les données : <a class="text-light"
                href="https://www.envidat.ch/dataset/fatal-avalanche-accidents-switzerland-1995">SLF (2018). Fatal avalanche
                accidents in Switzerland since 1995-1996. WSL Institute for Snow and Avalanche Research SLF. doi:
                10.16904/13.</a>
        </div>
    </div>
    `
};
