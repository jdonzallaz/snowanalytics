import RadarChart from "./RadarChart.js";
import { orientationLabels } from "../../const.js";

const options = {
  maintainAspectRatio: false,
  scale: {
    pointLabels: {
      fontColor: orientationLabels.map(lbl =>
        orientationLabels.indexOf(lbl) % 2 == 0 ? "#000" : "#aaa"
      ),
      fontSize: 12
    },
    ticks: {
      suggestedMax: 90
    }
  },
  legend: {
    display: false
  }
};

export default {
  name: "Orientation",
  components: { RadarChart },
  data: () => ({
    options
  }),
  computed: {
    data() {
      const data = this.$store.state.filteredData;

      const dataKey = "start.zone.slope.aspect";

      const radarData = [];

      for (const label of orientationLabels)
        radarData.push(data.filter(d => d[dataKey] === label).length);

      let radarDataNoHalfOrientation = Array(orientationLabels.length).fill(0); // Normalize with no NNE/SWW kind of orientation
      const labelsNoHalfOrientation = orientationLabels.filter(
        (val, i) => i % 2 == 0
      );

      for (const [i, value] of radarData.entries()) {
        if (i % 2 == 0) radarDataNoHalfOrientation[i] += value;
        else {
          radarDataNoHalfOrientation[i - 1] += Math.floor(value / 2);
          radarDataNoHalfOrientation[
            (i + 1) % orientationLabels.length
          ] += Math.ceil(value / 2);
        }
      }
      radarDataNoHalfOrientation = radarDataNoHalfOrientation.filter(
        (val, i) => i % 2 == 0
      );

      return {
        labels: labelsNoHalfOrientation,
        datasets: [
          {
            label: "Nombre d'accidents selon l'orientation de la pente",
            data: radarDataNoHalfOrientation,
            lineTension: 0.2
          }
        ]
      };
    },
    ...Vuex.mapState({})
  },
  template: `
    <div>
        <radar-chart :chartdata="data" :options="options" style="height: 300px;"/>
    </div>
    `
};
