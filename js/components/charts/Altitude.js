import LineChart from "./LineChart.js";
import { altitudeStep } from "../../const.js";

export default {
  name: "Altitude",
  components: { LineChart },
  props: ["separated", "ymax"],
  data() {
    const options = {
      maintainAspectRatio: false,
      elements: {
        line: {
          tension: 0.5
        }
      },
      scales: {
        xAxes: [
          {
            scaleLabel: {
              display: true,
              labelString: "Altitude [m]"
            }
          }
        ],
        yAxes: [
          {
            scaleLabel: {
              display: true,
              labelString: "Nombre d'accidents"
            },
            ticks: {
              suggestedMin: 0,
              suggestedMax: this.ymax || 40
            }
          }
        ]
      }
    };

    return {
      options: this.separated
        ? options
        : {
            ...options,
            legend: { display: false }
          }
    };
  },
  computed: {
    data() {
      const data = this.$store.state.filteredData;

      const dataKey = "start.zone.elevation";

      const labelsAltitude = [];
      for (
        let i = this.$store.state.filters.altitude[0];
        i <= this.$store.state.filters.altitude[1];
        i += altitudeStep
      )
        labelsAltitude.push(i);

      const altitudeData = [];
      for (const label of labelsAltitude)
        altitudeData.push(
          data.filter(
            d => d[dataKey] >= label && d[dataKey] < label + altitudeStep
          ).length
        );

      const altitudeDataOffpiste = [];
      const altitudeDataTour = [];
      for (const label of labelsAltitude) {
        altitudeDataOffpiste.push(
          data.filter(
            d =>
              d[dataKey] >= label &&
              d[dataKey] < label + altitudeStep &&
              d["activity"] == "offpiste"
          ).length
        );
        altitudeDataTour.push(
          data.filter(
            d =>
              d[dataKey] >= label &&
              d[dataKey] < label + altitudeStep &&
              d["activity"] == "tour"
          ).length
        );
      }
      if (this.separated)
        return {
          labels: labelsAltitude,
          datasets: [
            {
              label: "Hors-piste",
              data: altitudeDataOffpiste
            },
            {
              label: "Randonnée",
              data: altitudeDataTour
            }
          ]
        };
      return {
        labels: labelsAltitude,
        datasets: [
          {
            label:
              "Nombre d'accidents selon l'altitude de départ de l'avalanche",
            data: altitudeData
          }
        ]
      };
    },
    ...Vuex.mapState({})
  },
  template: `
    <div>
        <line-chart :chartdata="data" :options="options" style="height: 350px;"/>
    </div>
    `
};
