import { dangerLevels } from "../../const.js";
import { getMinY, getMaxY } from "../../utils.js";

Vue.component("apexchart", VueApexCharts);

const options = {
  plotOptions: {
    heatmap: {
      distributed: true,
      enableShades: true,
      shadeIntensity: 0.1,
    }
  },
  chart: {
    toolbar: {
      tools: {
        download: false
      }
    },
  },
  xaxis: {
    title: {
      text: "Année",
    },
  },
  yaxis: {
    title: {
      text: 'Danger d\'avalanche'
    }
  },
  colors: ["#44cc00","#ffcc00", "#ff6600", "#ff3300", "#800000"],
};

export default {
  name: "DangerLevelToDate",
  components: {},
  data: () => ({
    options
  }),
  computed: {
    data() {
      const data = this.$store.state.filteredData;

      if(data.length===0)
          return [];

      const dataKey = "forecasted.dangerlevel";

      const heatmapData = data
        .filter(d => !isNaN(d[dataKey]))
        .map(d => ({
          year: new Date(d["date"]).getFullYear(),
          danger: d[dataKey]
        }));

      if(heatmapData.length===0)
        return [];

      const series = [];

      for (const dangerLevel of dangerLevels.sort((a, b) => a.value - b.value)) {
        const s = [];
        for (
          let i = getMinY(heatmapData, "year");
          i <= getMaxY(heatmapData, "year");
          i++
        ) {
          s.push({
            x: i,
            y: heatmapData.filter(
              d => d.year == i && d.danger == dangerLevel.value
            ).length
          });
        }
        series.push({
          name: dangerLevel.name,
          data: s
        });
      }
      return series;
    },
    ...Vuex.mapState({})
  },
  template: `
    <div>
        <apexchart height="250" type="heatmap" :options="options" :series="data"></apexchart>
    </div>
    `
};
