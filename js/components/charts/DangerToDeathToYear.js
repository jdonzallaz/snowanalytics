import LineChart from "./LineChart.js";
import { dangerLevels } from "../../const.js";

const options = {
  maintainAspectRatio: false,
  elements: {
    line: {
      tension: 0.5
    }
  },
  scales: {
    xAxes: [
      {
        scaleLabel: {
          display: true,
          labelString: "Année"
        }
      }
    ],
    yAxes: [
      {
        scaleLabel: {
          display: true,
          labelString: "Nombre d'accidents pondéré par le danger"
        },
        ticks: {
          suggestedMin: 1,
          suggestedMax: 5
        }
      }
    ]
  }
};

export default {
  name: "DangerToDeathToYear",
  components: { LineChart },
  props: ["separated"],
  data() {
    return {
      options: this.separated
        ? options
        : {
            ...options,
            legend: { display: false }
          }
    };
  },
  computed: {
    data() {
      const data = this.$store.state.filteredData;

      const dataKey = "forecasted.dangerlevel";

      const labelsYear = [];
      for (
        let i = this.$store.state.filters.year[0];
        i <= this.$store.state.filters.year[1];
        i += 1
      ) {
        if (
          !this.separated &&
          data.filter(
            d => new Date(d["date"]).getFullYear() === i && !isNaN(d[dataKey])
          ).length
        )
          labelsYear.push(i);
        if (
          this.separated &&
          data.filter(
            d =>
              new Date(d["date"]).getFullYear() === i &&
              !isNaN(d[dataKey]) &&
              d["activity"] == "tour"
          ).length &&
          data.filter(
            d =>
              new Date(d["date"]).getFullYear() === i &&
              !isNaN(d[dataKey]) &&
              d["activity"] == "offpiste"
          ).length
        )
          labelsYear.push(i);
      }

      const levelData = [];
      const levelDataOffpiste = [];
      const levelDataTour = [];
      /*
      for (const label of labelsYear){
        let tempData = data.filter(d => new Date(d['date']).getFullYear() === label);
        let result = 0;
        let resultOffpiste = 0;
        let resultTour = 0;
        for (let dangerLevel of dangerLevels.sort((a, b) => a.value - b.value)) {
          result += tempData.filter(d => d[dataKey] == dangerLevel.value).length * dangerLevel.value;
          resultTour += tempData.filter(d => d[dataKey] == dangerLevel.value && d['activity'] == "tour").length * dangerLevel.value;
          resultOffpiste += tempData.filter(d => d[dataKey] == dangerLevel.value && d['activity'] == "offpiste").length * dangerLevel.value;
        }
        result /= data.filter(d => new Date(d['date']).getFullYear() === label).length;
        resultOffpiste /= data.filter(d => new Date(d['date']).getFullYear() === label && d['activity'] == "offpiste").length;
        resultTour /= data.filter(d => new Date(d['date']).getFullYear() === label && d['activity'] == "tour").length;
        levelData.push(result);
        levelDataOffpiste.push(resultOffpiste);
        levelDataTour.push(resultTour);
      }*/
      for (const label of labelsYear) {
        let tempData = data.filter(
          d => new Date(d["date"]).getFullYear() === label && !isNaN(d[dataKey])
        );
        let tempDataTour = data.filter(
          d =>
            new Date(d["date"]).getFullYear() === label &&
            !isNaN(d[dataKey]) &&
            d["activity"] == "tour"
        );
        let tempDataOffpiste = data.filter(
          d =>
            new Date(d["date"]).getFullYear() === label &&
            !isNaN(d[dataKey]) &&
            d["activity"] == "offpiste"
        );

        let result = 3;
        let resultOffpiste = 3;
        let resultTour = 3;
        for (let dangerLevel of dangerLevels.sort(
          (a, b) => a.value - b.value
        )) {
          let nbDead = tempData.filter(d => d[dataKey] == dangerLevel.value)
            .length;
          let nbDeadOffpiste = tempDataOffpiste.filter(
            d => d[dataKey] == dangerLevel.value
          ).length;
          let nbDeadTour = tempDataTour.filter(
            d => d[dataKey] == dangerLevel.value
          ).length;

          let dangerIdx = dangerLevel.value - 3;
          result += dangerIdx * (nbDead / tempData.length);
          resultTour += dangerIdx * (nbDeadTour / tempDataTour.length);
          resultOffpiste +=
            dangerIdx * (nbDeadOffpiste / tempDataOffpiste.length);
        }

        // Round to 2 decimal
        result = Math.round(result * 100) / 100;
        resultOffpiste = Math.round(resultOffpiste * 100) / 100;
        resultTour = Math.round(resultTour * 100) / 100;

        levelData.push(result);
        levelDataOffpiste.push(resultOffpiste);
        levelDataTour.push(resultTour);
      }

      if (this.separated)
        return {
          labels: labelsYear,
          datasets: [
            {
              label: "Hors-piste",
              data: levelDataOffpiste
            },
            {
              label: "Randonnée",
              data: levelDataTour
            }
          ]
        };
      return {
        labels: labelsYear,
        datasets: [
          {
            label: "Nombre d'accidents pondérés",
            data: levelData
          }
        ]
      };
    },
    ...Vuex.mapState({})
  },
  template: `
    <div>
        <line-chart :chartdata="data" :options="options" style="height: 350px;"/>
    </div>
    `
};
