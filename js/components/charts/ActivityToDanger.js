import BarChart from "./BarChart.js";
import { dangerLevels } from "../../const.js";

export default {
  name: "ActivityToDanger",
  components: { BarChart },
  props: ["year", "ymax"],
  computed: {
    options() {
      return {
        maintainAspectRatio: false,
        scales: {
          xAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Danger d'avalanche"
              }
            }
          ],
          yAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Nombre d'accidents"
              },
              ticks: {
                suggestedMin: 0,
                suggestedMax: this.ymax || 140
              }
            }
          ]
        }
      };
    },
    data() {
      let data = this.$store.state.filteredData;

      if (this.year)
        data = data.filter(
          d =>
            new Date(d["date"]).getFullYear() >= this.year[0] &&
            new Date(d["date"]).getFullYear() <= this.year[1]
        );

      const dataKey = "forecasted.dangerlevel";

      let dataOffpiste = [];
      let dataTour = [];

      for (let dangerLevel of dangerLevels.sort((a, b) => a.value - b.value)) {
        dataOffpiste.push(
          data.filter(
            d => d[dataKey] == dangerLevel.value && d["activity"] == "offpiste"
          ).length
        );
        dataTour.push(
          data.filter(
            d => d[dataKey] == dangerLevel.value && d["activity"] == "tour"
          ).length
        );
      }

      return {
        labels: dangerLevels.map(l => l.name),
        datasets: [
          {
            label: "Hors-piste",
            data: dataOffpiste
          },
          {
            label: "Randonnée",
            data: dataTour
          }
        ]
      };
    },
    ...Vuex.mapState({})
  },
  template: `
    <div>
        <bar-chart :chartdata="data" :options="options" style="height: 350px;"/>
    </div>
    `
};
