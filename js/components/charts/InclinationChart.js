import LineChart from "./LineChart.js";

export default {
  name: "InclinationChart",
  components: { LineChart },
  props: ["separated", "ymax"],
  data() {
    const options = {
      maintainAspectRatio: false,
      elements: {
        line: {
          tension: 0.5
        }
      },
      scales: {
        xAxes: [
          {
            scaleLabel: {
              display: true,
              labelString: "Inclinaison [degré]"
            }
          }
        ],
        yAxes: [
          {
            scaleLabel: {
              display: true,
              labelString: "Nombre d'accidents"
            },
            ticks: {
              suggestedMin: 0,
              suggestedMax: this.ymax || 40
            }
          }
        ]
      }
    };

    return {
      options: this.separated
        ? options
        : {
            ...options,
            legend: { display: false }
          }
    };
  },
  computed: {
    data() {
      const data = this.$store.state.filteredData;

      const dataKey = "start.zone.inclination";

      const labelsInclination = [];
      for (
        let i = this.$store.state.filters.inclination[0];
        i <= this.$store.state.filters.inclination[1];
        i += 1
      )
        labelsInclination.push(i);

      const inclinationData = [];
      for (const label of labelsInclination)
        inclinationData.push(data.filter(d => d[dataKey] === label).length);

      const inclinationDataOffpiste = [];
      const inclinationDataTour = [];
      for (const label of labelsInclination) {
        inclinationDataOffpiste.push(
          data.filter(d => d[dataKey] == label && d["activity"] == "offpiste")
            .length
        );
        inclinationDataTour.push(
          data.filter(d => d[dataKey] == label && d["activity"] == "tour")
            .length
        );
      }

      if (this.separated)
        return {
          labels: labelsInclination,
          datasets: [
            {
              label: "Hors-piste",
              data: inclinationDataOffpiste
            },
            {
              label: "Randonnée",
              data: inclinationDataTour
            }
          ]
        };
      return {
        labels: labelsInclination,
        datasets: [
          {
            label: "Nombre d'accidents selon l'inclinaison de la pente",
            data: inclinationData
          }
        ]
      };
    },
    ...Vuex.mapState({})
  },
  template: `
    <div>
        <line-chart :chartdata="data" :options="options" style="height: 350px;"/>
    </div>
    `
};
