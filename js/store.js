import { data } from "./data.js";
import { getMinY, getMaxY, filterDataset } from "./utils.js";

export default new Vuex.Store({
  state: {
    data,
    filtersInit:{
      init: false,
      inclination: null,
      danger: null,
      year: null,
      altitude: null,
      activity: {
        offpiste: true,
        tour: true,
        other: true,
      },
    },
    filters: null,
    filteredData: data,
    selectedHypothesis: 0,
    hypothesis: [
      {
        title: "Exploration libre",
        id: 0,
        filter: {}
      },
      {
        title: "Quelles sont les pentes les plus dangereuses ?",
        id: 1,
        filter: {}
      },
      {
        title: "Les types de skieurs jugent-ils différemment les conditions ?",
        id: 2,
        filter: {}
      },
      {
        title: "Les gens sont-ils mieux éduqués qu'auparavant ?",
        id: 3,
        filter: {}
      }
    ]
  },
  mutations: {
    selectHypothesis(state, payload) {
      state.selectedHypothesis = payload.id;
    },
    initFilters(state) {
      const year = [new Date(getMinY(state.data, "date")).getFullYear(),
                    new Date(getMaxY(state.data, "date")).getFullYear()];
      state.filtersInit.year = year;

      state.filtersInit.danger = [1, 5];

      let dataKey = "start.zone.inclination";
      let data = state.data.filter(d => !isNaN(d[dataKey]))
      let minInc = getMinY(data, dataKey);
      let maxInc = getMaxY(data, dataKey);
      state.filtersInit.inclination = [minInc, maxInc];

      dataKey = "start.zone.elevation";
      data = state.data.filter(d => !isNaN(d[dataKey]))
      let minAlt = getMinY(data, dataKey);
      let maxAlt = getMaxY(data, dataKey);
      if (!Number.isInteger(minAlt/10))
          minAlt = parseInt(minAlt/10) *10
      state.filtersInit.altitude = [minAlt, maxAlt];

      state.filters = JSON.parse(JSON.stringify(state.filtersInit));
      state.filtersInit.init = true;
    },
    setFilter(state, payload) {
      switch (payload.filter){
        case "inclination":
          state.filters.inclination = payload.value;
          break;
        case "altitude":
            state.filters.altitude = payload.value;
            break;
        case "year":
          state.filters.year = payload.value;
          break;
        case "danger":
            state.filters.danger = payload.value;
            break;
        case "activity":
            switch (payload.activity){
              case "offpiste":
                  state.filters.activity.offpiste = payload.value;
                  break;
              case "tour":
                  state.filters.activity.tour = payload.value;
                  break;
              case "other":
                  state.filters.activity.other = payload.value;
                  break;
            }
            break;
      }

      // EXECUTE THE FILTERS ON THE DATASET
      if (state.filtersInit.init){
        let data = filterDataset(state.data, state.filters, state.filtersInit);
        state.filteredData = data;
      }
    },
    resetFilters(state) {
      state.filters = JSON.parse(JSON.stringify(state.filtersInit));
      if (state.filtersInit.init){
        let data = filterDataset(state.data, state.filters, state.filtersInit);
        state.filteredData = data;
      }
    },
  }
});
