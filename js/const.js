export const dangerLevels = [
  { name: "Faible", value: 1, color: "#b3d245" },
  { name: "Limité", value: 2, color: "#eae738" },
  { name: "Marqué", value: 3, color: "#f6932c" },
  { name: "Fort", value: 4, color: "#ec1c24" },
  { name: "Très fort", value: 5, color: "#9b1117" }
];

export const altitudeStep = 100;

export const orientationLabels = [
  "N",
  "NNE",
  "NE",
  "NEE",
  "E",
  "SEE",
  "SE",
  "SSE",
  "S",
  "SSW",
  "SW",
  "SWW",
  "W",
  "NWW",
  "NW",
  "NNW"
];

export const primaryColor = "rgba(77, 179, 255, 0.3)";

export const fontfamily =
  '-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji"';
