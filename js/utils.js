export function getMinY(data, key) {
  return data.reduce((min, p) => (p[key] < min ? p[key] : min), data[0][key]);
}

export function getMaxY(data, key) {
  return data.reduce((max, p) => (p[key] > max ? p[key] : max), data[0][key]);
}

export function filterDataset(data, filters, init) {
  if (filters.altitude === null || filters.inclination === null) 
    return data;

    
  data = data.filter(d =>{
    // Filter by activity
    let key = "activity";
    if(!filters.activity.offpiste && d[key] == "offpiste") return false;
    if(!filters.activity.tour && d[key] == "tour") return false;
    if(!filters.activity.other && (d[key] == "transportation.corridor" || d[key] == "other, mixed or unknown" || d[key] == "building")) return false;


    // Filter by danger
    key="forecasted.dangerlevel"
    if (filters.danger[0]!==init.danger[0] || filters.danger[1]!==init.danger[1])
      if (d[key]=="NA" || 
        parseInt(d[key]) < filters.danger[0] 
        || parseInt(d[key]) > filters.danger[1])
          return false;


    // Filter by altitude
    key="start.zone.elevation"
    if (filters.altitude[0]!==init.altitude[0] || filters.altitude[1]!==init.altitude[1])
      if (d[key]=="NA" || parseInt(d[key]) < filters.altitude[0] || parseInt(d[key]) > filters.altitude[1])
        return false;

    // Filter by inclination
    key="start.zone.inclination"
    if (filters.inclination[0]!==init.inclination[0] || filters.inclination[1]!==init.inclination[1])
      if (d[key]=="NA" || parseInt(d[key]) < filters.inclination[0] || parseInt(d[key]) > filters.inclination[1])
        return false;

    // Filter by year
    key="date"
    if (filters.year[0]!==init.year[0] || filters.year[1]!==init.year[1])
      if (new Date(d[key]).getFullYear() < filters.year[0] || new Date(d[key]).getFullYear() > filters.year[1])
        return false;

    return true
  });

  return data;
}
