import App from "./components/App.js";
import Explore from "./components/Explore.js";
import Details from "./components/Details.js";
import Doc from "./components/Doc.js";
import store from "./store.js";
import { fontfamily } from "./const.js";

Chart.defaults.global.defaultFontFamily = fontfamily;
Chart.defaults.global.plugins.colorschemes.scheme = "tableau.ClassicLight10";
Vue.component("vue-slider", window["vue-slider-component"], {
  name: "vue-slider"
});

const routes = [
  { path: "/", redirect: "/explore" },
  { path: "/explore", component: Explore },
  { path: "/details", component: Details },
  { path: "/doc", component: Doc }
];

const router = new VueRouter({ routes });

new Vue({
  el: "#app",
  render: h => h(App),
  store,
  router,
  components: {
    VueSlider: window["vue-slider-component"]
  }
});
